import 'package:flutter/material.dart';

class EventPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Container(
          child: new Center(
              child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new IconButton(
            icon: new Icon(Icons.local_pizza, color: Colors.redAccent),
            iconSize: 70.0,
            onPressed: () {
              Navigator.of(context).pushNamed("/PizzaPage");
            },
          ),
          new Text("Pizza Day!")
        ],
      ))),
    );
  }
}