import 'package:flutter/material.dart';
import './pizza_page.dart';
import './home_page.dart' as first;
import './events_page.dart' as second;
import './places_pages.dart' as third;

void main() {
  runApp(new MaterialApp(
    home: new TestHome(),
    routes: <String, WidgetBuilder>{
      "/PizzaPage": (BuildContext context) => new PizzaPage()
    },
  ));
}

class TestHome extends StatefulWidget {
  @override
  _TestHomeState createState() => new _TestHomeState();
}

class _TestHomeState extends State<TestHome>
    with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 3);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("NASY"),
        backgroundColor: Colors.cyan[400],
        
        bottom: new TabBar(
          controller: controller,
          tabs: <Widget>[
            new Tab(
              icon: new Icon(Icons.home),
            ),
            new Tab(
              icon: new Icon(Icons.location_on),
            ),
            new Tab(
              icon: new Icon(Icons.event_note),
            )
          ],
        ),
      ),

    
      body: new TabBarView(controller: controller, children: <Widget>[
        new first.HomePage(),
        new second.EventPage(),
        new third.PlacesPage(),
      ]),
  
    );
  }
}
