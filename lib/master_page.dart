import 'package:flutter/material.dart';

class MasterPage extends StatefulWidget {
  @override
  _MasterPageState createState() => new _MasterPageState();
}

class _MasterPageState extends State<MasterPage> {
  
  List<Container> daftarSuperhero = new List();

  var karakter = [
    {"nama": "Ipnone 5", "gambar": "iphone1.jpg"},
    {"nama": "Iphone 5s", "gambar": "iphone2.jpg"},
    {"nama": "Iphone 7", "gambar": "iphone3.jpg"},
    {"nama": "Iphone 8", "gambar": "iphone4.jpg"},
    {"nama": "Iphone X", "gambar": "iphone5.png"},
    {"nama": "Ipnone 5", "gambar": "iphone1.jpg"},
    {"nama": "Iphone 5s", "gambar": "iphone2.jpg"},
    {"nama": "Iphone 7", "gambar": "iphone3.jpg"},
    {"nama": "Iphone 8", "gambar": "iphone4.jpg"},
    {"nama": "Iphone X", "gambar": "iphone5.png"},
  ];

  _buatlist() async {
    for (var i = 0; i < karakter.length; i++) {
      final karakternya = karakter[i];
      final String gambar = karakternya["gambar"];

      daftarSuperhero.add(new Container(
          padding: new EdgeInsets.all(10.0),
          child: new Card(
              child: new Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10.0),
              ),
              new Image.asset(
                "assets/products/$gambar",
                fit: BoxFit.cover,
                height: 100.0,
              ),
              new Text(
                karakternya['nama'],
                style: new TextStyle(fontSize: 20.0),
              )
            ],
          ))));
    }
  }

  @override
  void initState() {
    super.initState();
    _buatlist();
  }

  @override
  Widget build(BuildContext context) {
    return new GridView.count(
      primary: true,
      crossAxisCount: 2,
      children: daftarSuperhero,
    );
  }
}
