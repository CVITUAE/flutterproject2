import 'package:flutter/material.dart';

class PizzaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pizza Event!"),
        backgroundColor: Colors.deepOrange,
      ),
      body: new Container(
          child: new Center(
              child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new IconButton(
              icon: new Icon(Icons.home, color: Colors.redAccent),
              iconSize: 70.0,
              onPressed: null),
          new Text("Hello World")
        ],
      ))),
    );
  }
}
