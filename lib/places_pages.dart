import 'package:flutter/material.dart';

class PlacesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Container(
          child: new Center(
              child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new IconButton(
            icon: new Icon(Icons.local_movies, color: Colors.redAccent),
            iconSize: 70.0,
            onPressed: null
          ),
          new Text("Surprise Places")
        ],
      ))),
    );
  }
}